#pragma once

#include <QtWidgets/QGraphicsView>
#include <QGraphicsScene>
#include <QHash>

bool operator<(const QColor& l, const QColor& r) noexcept;

uint qHash(const QColor& col) noexcept;

class TMyGraphicsScene : public QGraphicsScene {
    virtual void drawBackground(QPainter* painter, const QRectF& /*rect*/) override {
        painter->save();
        for (auto it = Points.begin(); it != Points.end();  ++it) {
            painter->setPen(it.key());
            for (const QPointF& point : it.value())
                painter->drawPoint(point);
        }
        painter->restore();
    }

public:
    TMyGraphicsScene(QWidget* parent = 0)
        : QGraphicsScene(parent)
    {}

public:
    QHash<QColor, QVector<QPointF>> Points;
};

class MyGraphicsView : public QGraphicsView {
    Q_OBJECT

public:
    explicit MyGraphicsView(QWidget *parent = 0);
    void Redraw() {
        Scene.update();
    }

public slots:
    void SetColor(const int color) {
        Color = (Qt::GlobalColor) color;
    }
    void Clear() {
        Scene.clear();
        Scene.Points.clear();
        update();
    }

    void AddAreaPoint(const QPointF& point, const QColor& color);
    void AddPoint(const QPointF& point);

signals:
    void NewPoint(const QPointF&, const Qt::GlobalColor);

private:
    virtual void mousePressEvent(QMouseEvent* event) override;

private:
    TMyGraphicsScene Scene;
    Qt::GlobalColor Color;
};
