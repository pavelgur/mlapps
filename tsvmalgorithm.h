#pragma once

#include "IAlgorithm.h"
#include "matrix.h"

class TSvmAlgorithm : public IAlgorithm {
public:
    TSvmAlgorithm(const TSample& learnSet, const qreal& cParameter = 100);

    void Train() override;

    qreal Apply(const TVector &x) const noexcept override;

    QString GetName() const noexcept override {
        return  "RBF SVM";
    }

private:
    inline qreal Kernel(const TVector& x1, const TVector& x2);

private:
    const qreal ParameterC;
    const qreal ParameterKernel;
    QMap<qreal, QPair<TVector, qreal>> Omegas; // Omega, Omega0
};
