#-------------------------------------------------
#
# Project created by QtCreator 2014-11-01T23:09:58
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MLalgorithms
CONFIG   += c++11

TEMPLATE = app


SOURCES += main.cpp \
    temalgorithm.cpp \
    alglib/alglibinternal.cpp \
    alglib/alglibmisc.cpp \
    alglib/ap.cpp \
    alglib/linalg.cpp \
    matrix.cpp \
    mygraphicsview.cpp \
    mainw.cpp \
    tsvmalgorithm.cpp \
    knnalgorithm.cpp
#    tdecisionforest.cpp

HEADERS += \
    lib.h \
    temalgorithm.h \
    alglib/alglibinternal.h \
    alglib/alglibmisc.h \
    alglib/ap.h \
    alglib/linalg.h \
    alglib/stdafx.h \
    matrix.h \
    myexception.h \
    mygraphicsview.h \
    IAlgorithm.h \
    mainw.h \
    tsvmalgorithm.h \
    knnalgorithm.h
#    tdecisionforest.h

FORMS += \
    mainw.ui
