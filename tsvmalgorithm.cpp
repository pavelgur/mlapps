#include "tsvmalgorithm.h"
#include <cfloat>
#include <limits>

TSvmAlgorithm::TSvmAlgorithm(const TSample& learnSet, const qreal& cParameter)
    : IAlgorithm(learnSet)
    , ParameterC(cParameter)
    , ParameterKernel([&] () {
    if (LearnSet.size() == 0)
        return 0.;

    long double res = 0;
    for (const auto& pair : LearnSet) {
        res += pair.first(pair.first);
    }

    return qreal(LearnSet.size() / res);
} ())
{}

qreal TSvmAlgorithm::Kernel(const TVector& x1, const TVector& x2) {
    const TVector dx = x1 - x2;
    const qreal res = exp(-ParameterKernel * dx(dx));
    return res;
}

void TSvmAlgorithm::Train() {
    if (LearnSet.size() < 2) {
        qDebug() << "Cannot train on too small set!";
        return;
    }

    QSet<qreal> classes;
    for (size_t i = 0 ; i < (size_t) LearnSet.size(); ++i) {
        classes << LearnSet[i].second;
    }

    if (classes.size() <= 1) {
        qDebug() << "Cannot train: too few classes!";
        return;
    }

    for (const auto& classId : classes) {
        auto omegaIt = Omegas.insert(classId, QPair<TVector, qreal>(TVector(LearnSet.front().first.GetLength(), 0), 0));
        QSet<size_t>
                  sSet // support objects, m[i] = 1, 0 < lambda[i] < C
                , oSet // peripheral objects, m[i] > 1, lambda[i] = 0
                , cSet; // objects-offenders, m[i] < 1, lambda[i] = C
        {
            size_t firstBest = 0, secondBest = 0;
            auto bestDist = std::numeric_limits<qreal>::max();

            for (auto i = 0u; i < (size_t) LearnSet.size() - 1; ++i) {
                for (auto j = i + 1u; j < (size_t) LearnSet.size(); ++j) {
                    if (LearnSet[i].second == LearnSet[j].second)
                        continue;

                    const TVector dx = LearnSet[i].first - LearnSet[j].first;
                    const qreal dist = dx(dx);

                    if (dist < bestDist) {
                        bestDist = dist;
                        firstBest = i;
                        secondBest = j;
                    }
                }
            }
            sSet << firstBest << secondBest;
            for (auto i = 0u; i < size_t(LearnSet.size()); ++i) {
                if (!sSet.contains(i))
                    oSet << i;
            }
        }

        auto belongsToClass = [&] (const qreal& c) noexcept {
            return (c == classId ? 1. : -1.);
        };

        forever {
            TVector lambdas;

            while (sSet.size() > 0) { // the quadratic programming problem starts here
                lambdas = TVector(sSet.size(), 0);
                TMatrix qCS(qMax(cSet.size(), 1), sSet.size(), 0);
                TMatrix slauA(sSet.size() + 1, sSet.size() + 1, 0);
                auto it2 = sSet.cbegin();

                for (auto j = 0u; j < (size_t) sSet.size(); ++j, ++it2) {
                    const auto& l2 = LearnSet[*it2];
                    slauA[j][sSet.size()] = slauA[sSet.size()][j] = belongsToClass(l2.second);

                    {
                        auto it1 = sSet.cbegin();
                        for (auto i = 0u; i < size_t(sSet.size()); ++i, ++it1) {
                            const auto& l1 = LearnSet[*it1];
                            slauA[i][j] = belongsToClass(l1.second) * belongsToClass(l2.second) * Kernel(l1.first, l2.first);
                        }
                    }
                    {
                        auto it1 = cSet.cbegin();
                        for (size_t i = 0; i < (size_t) cSet.size(); ++i, ++it1) {
                            const auto& l1 = LearnSet[*it1];
                            qCS[i][j] = belongsToClass(l1.second) * belongsToClass(l2.second) * Kernel(l1.first, l2.first);
                        }
                    }
                }
                TVector yC(qMax(cSet.size(), 1), 0);
                {
                    size_t index = 0;
                    for (auto it = cSet.begin(); it != cSet.end(); ++it, ++index) {
                        yC[index] = belongsToClass(LearnSet[*it].second);
                    }
                }

                auto slauB = TVector::MakeUnitaryVector(sSet.size());

                if (cSet.size() > 0)
                    slauB -= qCS.Transposed() * TVector::MakeUnitaryVector(cSet.size()) * ParameterC;

                if (cSet.empty())
                    slauB << 0;
                else
                    slauB << (TVector::MakeUnitaryVector(cSet.size())(yC) * (-ParameterC));

                const TVector res = slauA.Inverted() * slauB; // longer than lambdas!
                for (size_t i = 0; i < lambdas.GetLength(); ++i) {
                    lambdas[i] = res[i];
                }

                bool transferedSmth = false;
                if (sSet.size() > 2) {
                    size_t index = 0;
                    for (auto it = sSet.begin(); it != sSet.end(); ++index) {
                        const bool cond1 = lambdas[index] <= 0;
                        const bool cond2 = lambdas[index] >= ParameterC;
                        if (cond1) {
                            oSet << *it;
                        }
                        else if (cond2) {
                            cSet << *it;
                        }
                        if (cond1 || cond2) {
                            it = sSet.erase(it);
                            transferedSmth = true;
                        } else
                            ++it;
                    }
                }
                if (!transferedSmth)
                    break;
            }

            omegaIt.value().first = TVector(LearnSet.front().first.GetLength(), 0);
            {
                size_t i = 0;
                for (auto it = sSet.begin(); it != sSet.end(); ++i, ++it) {
                    omegaIt.value().first += LearnSet[*it].first * belongsToClass(LearnSet[*it].second) * lambdas[i];
                }
            }

            for (const auto& i : cSet) {
                omegaIt.value().first += LearnSet[i].first * belongsToClass(LearnSet[i].second) * ParameterC;
            }

            {
                QVector<qreal> values;
                values.reserve(LearnSet.size());
                for (size_t i : sSet.unite(cSet)) {
                    const auto& pair = LearnSet[i];
                    values.push_back(omegaIt.value().first(pair.first) - belongsToClass(pair.second));
                }
                std::sort(values.begin(), values.end());
                omegaIt.value().second = (values[qMax(0, (values.size() - 1) / 2)] +
                        values[qMin(values.size() - 1, (values.size() - 1) / 2 + 1)]) / 2;
            }
            if (oSet.empty() && cSet.empty())
                break;

            QVector<qreal> margins;
            margins.reserve(LearnSet.size());
            for (const auto& pair : LearnSet) {
                margins.push_back(belongsToClass(pair.second) * (omegaIt.value().first(pair.first) - omegaIt.value().second));
            }

            bool transferedSmth = false;
            for (auto it = oSet.begin(); it != oSet.end();) {
                if  (margins[*it] <= 1) {
                    sSet << *it;
                    it = oSet.erase(it);
                    transferedSmth = true;
                } else
                    ++it;
            }
            for (auto it = cSet.begin(); it != cSet.end();) {
                if  (margins[*it] >= 1) {
                    sSet << *it;
                    it = cSet.erase(it);
                    transferedSmth = true;
                } else
                    ++it;
            }
            if (!transferedSmth)
                break;
        }
    }
    IAlgorithm::Train();
}

qreal TSvmAlgorithm::Apply(const TVector &x) const noexcept {
    qreal bestClass = (Qt::GlobalColor) int(LearnSet.front().second);
    qreal bestMargin = std::numeric_limits<double>::lowest();

    for (auto omega = Omegas.begin(); omega != Omegas.end(); ++omega) {
        const qreal margin = omega.value().first(x) - omega.value().second;
        if (margin > bestMargin) {
            bestMargin = margin;
            bestClass = omega.key();
        }
    }
    return bestClass;
}
