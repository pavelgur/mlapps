#pragma once

#include <QtWidgets/QWidget>
#include <QtWidgets/QButtonGroup>

#include "lib.h"

namespace Ui {
class MainW;
}

class MainW : public QWidget {
    Q_OBJECT

    enum EAlgo {
        A_EmRegression = 0,
        A_KnnClassification = 1,
        A_RbfSvmRegression = 2,
    };

public:
    explicit MainW(QWidget *parent = 0);
    ~MainW();

private slots:
    void GetPoint(const QPointF &pos, const Qt::GlobalColor color);
    void Clear();
    void RunAlgo();
    void ChangeAlgo(const int index) {CurAlgoChoice = (MainW::EAlgo) index;}

private:
    Ui::MainW *ui;
    QButtonGroup ButtonGroup;
    TSample Sample;
    EAlgo CurAlgoChoice = A_EmRegression;
};
