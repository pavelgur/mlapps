#include "temalgorithm.h"

#include  <QtMath>
#include <QDebug>

TEmAlgorithm::TEmAlgorithm(const TSample& learnSet)
    : IAlgorithm(learnSet)
    , Dimension(learnSet.front().first.GetRows())
{
    for (const auto& pair : LearnSet) {
        ClassToData[pair.second].Dots.push_back(&pair.first);
    }
    for (auto cl = ClassToData.begin(); cl != ClassToData.end(); ++cl) {
        cl->MinObjectLeft = qMax(2, cl->Dots.size() / 20);
        if ((decltype(cl->MinObjectLeft)) cl->Dots.size() <= cl->MinObjectLeft)
            qDebug() << "Warning:" << (Qt::GlobalColor) (int) cl.key() << "skipped due to too few points";
    }
}

qreal TEmAlgorithm::Apply(const TVector& x) const noexcept {
    auto bestClass = static_cast<qreal>(Qt::white);
    auto bestClassProb = 0.0;
    for (auto data = ClassToData.begin(); data != ClassToData.end(); ++data) {
        if (!data->Calculated)
            continue;
        const auto prob = ObjectLikelihood(data.value(), x);
        if (prob > bestClassProb) {
            bestClassProb = prob;
            bestClass = data.key();
        }
    }

    return bestClass;
}

void TEmAlgorithm::Train() {
    for (auto data = ClassToData.begin(); data != ClassToData.end(); ++data) {
        qDebug() << "Class:" << data.key();
        auto enough = false;
        while (!enough) {
            auto maxLikelihood = 0.0;
            for (const auto* dot : data->Dots) {
                maxLikelihood = qMax(maxLikelihood, ObjectLikelihood(data.value(), *dot));
            }

            QVector<const TVector*> leftObjects;
            for (const auto* dot : data->Dots) {
                if (ObjectLikelihood(data.value(), *dot) <= maxLikelihood / StopCoef)
                    leftObjects.push_back(dot);
            }

            if (static_cast<decltype(data->MinObjectLeft)>(leftObjects.size()) < data->MinObjectLeft)
                break;

            AddComponent(static_cast<qreal>(leftObjects.size()) / data->Dots.size(), leftObjects, data.value());

            // EM algo
            QVector<QVector<qreal>> gMatrix(leftObjects.size(), QVector<qreal>(data->Omega.size(), 0));
            auto maxGchange = 0.0;
            while (!enough) { // EM steps until process converges

                // Expectation
                maxGchange = 0;
                for (auto i = 0; i < static_cast<decltype(i)>(leftObjects.size()); ++i) {
                    auto ratio = 0.0;
                    QVector<qreal> newValues(gMatrix[i].size(), 0);
                    for (auto j = 0u; j < (decltype(j)) data->Omega.size(); ++j) {
                        newValues[j] = data->Omega[j] * Phi(*leftObjects[i], data->Theta[j]);
                        ratio += newValues[j];
                    }
                    for (auto j = 0u; j < static_cast<decltype(j)>(data->Omega.size()); ++j) {
                        const auto newGValue = newValues[j] / ratio;
                        maxGchange = qMax(maxGchange, qAbs(gMatrix[i][j] - newGValue));
                        gMatrix[i][j] = newGValue;
                    }
                }

                // Maximization
                for (auto j = 0u; j < static_cast<decltype(j)>(data->Omega.size());) {
                    QVector<bool> skipPoint(leftObjects.size(), true);
                    srand(time(nullptr));
                    auto matchedPoints = 0u;
                    for (auto i = 0u; i < (decltype(i)) skipPoint.size(); ++i) {
                        if (1.0 * rand() / RAND_MAX <= gMatrix[i][j]) {
                            skipPoint[i] = false;
                            ++matchedPoints;
                        }
                    }
                    if (matchedPoints < data->MinObjectLeft) { // компонента слишком малочисленна, удаляется
                        qDebug() << "removing component";
                        data->Omega.remove(j);
                        data->Theta.remove(j);
                        for (auto& vec : gMatrix) {
                            vec.remove(j);
                        }
                        enough = true; // компоненты больше добавляться не будут
                        // индекс j уже на следующей компоненте, не надо инкрементироваться
                    } else {
                        Optimize(leftObjects, data.value(), j, skipPoint);
                        data->Omega[j] = 0;
                        for (auto i = 0u; i < static_cast<decltype(i)>(leftObjects.size()); ++i) {
                            data->Omega[j] += gMatrix[i][j];
                        }
                        data->Omega[j] /= leftObjects.size();
                        ++j;
                    }
                }
                if (maxGchange < Delta)
                    break;
            }
            if (enough)
                break;
        }
        data->Calculated = true;
        for (const auto* dot : data->Dots) {
            data->ClassPower += ObjectLikelihood(data.value(), *dot);
        }
        data->ClassPower /= data->Dots.size();
    }
    IAlgorithm::Train();
}

qreal TEmAlgorithm::Phi(const TVector& x, const TGaussianDist& dist) const {
    const auto v1 = x - dist.Mu;
    const auto v2 = dist.Sigma + TMatrix::MakeUnitaryMatrix(dist.Sigma.GetRows()) * (qreal) (3 + rand() % 16); // an attempt to make it well-conditioned
//    const auto v2 = dist.Sigma;
    return exp(-0.5 * (v1.Transposed() * v2.Inverted() * v1)[0][0]) /
            (pow(M_2_PI, (qreal) Dimension / 2) * sqrt(v2.Determinant()));
}

void TEmAlgorithm::Optimize(const QVector<const TVector*>& dots, TEmAlgorithm::TClassData& data, const size_t j, const QVector<bool>& skipPoint) {
    auto& dist = data.Theta[j];
    dist = TGaussianDist(Dimension);
    for (auto i = 0u; i < (decltype(i)) dots.size(); ++i) {
        if (skipPoint[i])
            continue;
        dist.Mu = dist.Mu * i / (1.0 + i) + *dots[i] / (1.0 + i);
    }
    dist.Mu /= data.Omega[j];
    for (auto i = 0u; i < (decltype(i)) dots.size(); ++i) {
        if (skipPoint[i])
            continue;
        const auto a = *dots[i] - dist.Mu;
        dist.Sigma = dist.Sigma * i / (1.0 + i) + a * a.Transposed() / (1.0 + i);
    }
    dist.Sigma /= data.Omega[j];
}

void TEmAlgorithm::AddComponent(const qreal& newComponentWeight, const QVector<const TVector*>& dots, TClassData& data) {
    qDebug() << "Adding component";
    data.Omega.push_back(newComponentWeight);
    data.Theta.push_back(TGaussianDist(Dimension));
    Optimize(dots, data, data.Omega.size() - 1, QVector<bool>(dots.size(), false));
}

qreal TEmAlgorithm::ObjectLikelihood(const TClassData& data, const TVector& x) const noexcept {
    if (data.Omega.empty())
        return 0;
    auto sum = 0.0;
    for (auto i = 0u; i < (decltype(i)) data.Omega.size(); ++i) {
        sum += Phi(x, data.Theta[i]) * data.Omega[i];
    }
    return sum;
}
