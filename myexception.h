#pragma once

#include <exception>
#include <QString>

class myexception : public std::exception {
public:
    myexception(const QString& what = "")
        :WAT(what)
    {
    }

    ~myexception() noexcept {}
    const char* what() const noexcept {
        return WAT.toLocal8Bit().data();
    }
    myexception& operator<< (const QString& s) {
        WAT.append(s);
        return *this;
    }

private:
    QString WAT;
};
