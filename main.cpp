#include "mainw.h"
#include <QtWidgets/QApplication>
#include <QScreen>

int main(int argc, char* argv[]) {
    QApplication app(argc, argv);
    MainW w;
    w.move(app.screens().front()->geometry().center() - w.rect().center());
    w.show();

    return app.exec();
}
