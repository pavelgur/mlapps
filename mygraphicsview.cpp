#include "mygraphicsview.h"

#include <QtWidgets/QtWidgets>
#include <QDebug>

template <typename TOne>
static uint MultiHash(const TOne& one) noexcept {
    return qHash(one);
}

template <typename THead, typename ... TTail>
static uint MultiHash(const THead& one, TTail&& ... tail) noexcept {
    return qHash(MultiHash(tail ...)) ^ qHash(one);
}

uint qHash(const QColor& col) noexcept {
    return MultiHash(col.alpha(), col.black(), col.blue(), col.red(), col.green());
}

MyGraphicsView::MyGraphicsView(QWidget *parent)
    : QGraphicsView(parent)
    , Scene(this)
    , Color(Qt::red)
{
    setScene(&Scene);
    Scene.setSceneRect(rect());
    Scene.setItemIndexMethod(QGraphicsScene::NoIndex);
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

    setOptimizationFlags(QGraphicsView::DontSavePainterState | QGraphicsView::DontAdjustForAntialiasing);
    setCacheMode(QGraphicsView::CacheNone);
}

void MyGraphicsView::AddAreaPoint(const QPointF& point, const QColor& color) {
    Scene.Points[color].push_back(point);
}

void MyGraphicsView::AddPoint(const QPointF &point) {
    const QColor col(Color);
    Scene.addEllipse(point.x(), point.y(), 6, 6, QPen(Qt::black, 1), QBrush(col, Qt::SolidPattern));
    emit NewPoint(point, Color);
}

void MyGraphicsView::mousePressEvent(QMouseEvent* event) {
    AddPoint(mapToScene(mapFromGlobal(event->globalPos())));
}

bool operator<(const QColor& l, const QColor& r) noexcept {
#define COMPARE_RETURN(FIELD)         \
    if (l.FIELD() != r.FIELD())       \
        return l.FIELD() < r.FIELD();

    COMPARE_RETURN(alpha)
    COMPARE_RETURN(red)
    COMPARE_RETURN(green)
    COMPARE_RETURN(blue)
    return false;

#undef COMPARE_RETURN
}
