#pragma once

#include <QVector>
#include <QPair>

#include "myexception.h"
#include "alglib/linalg.h"

class TMatrix {
public:
    TMatrix() = default;

    TMatrix(const TMatrix& other) noexcept
        : Data(other.Data)
        , Rows(other.Rows)
        , Columns(other.Columns)
    {}

    TMatrix(const size_t rows, const size_t columns, const qreal value = 0);

    TMatrix(const alglib::real_2d_array& data) noexcept
        : Data(data)
        , Rows(data.rows())
        , Columns(data.cols())
    {}

    const double* operator[](const size_t i) const noexcept {
        return Data[i];
    }

    double* operator[](const size_t i) noexcept {
        return Data[i];
    }

    size_t GetRows() const noexcept {
        return Rows;
    }
    size_t GetColumns() const noexcept {
        return Columns;
    }

    static TMatrix MakeUnitaryString(const size_t length) noexcept {
        return TMatrix(1, length, 1);
    }

    static TMatrix MakeUnitaryMatrix(const size_t rank) noexcept {
        TMatrix e(rank, rank, 0);
        for (size_t i = 0; i < rank; ++i) {
            e[i][i] = 1;
        }
        return e;
    }

    TMatrix Transposed() const noexcept;
    TMatrix Inverted() const;

    qreal Determinant() const;

    TMatrix& operator*=(const qreal& mult) noexcept;
    TMatrix& operator/=(const qreal& mult) noexcept;
    TMatrix& operator+=(const qreal& m) noexcept;
    TMatrix& operator-=(const qreal& m) noexcept;

    TMatrix& operator+=(const TMatrix& m);
    TMatrix& operator-=(const TMatrix& m);
    TMatrix& operator*=(const TMatrix& m);

    friend TMatrix operator+(TMatrix a, const TMatrix& b) noexcept;
    friend TMatrix operator-(TMatrix a, const TMatrix& b) noexcept;
    friend TMatrix operator*(TMatrix a, const TMatrix& b) noexcept;
    friend TMatrix operator*(TMatrix a, const qreal& b) noexcept;
    friend TMatrix operator/(TMatrix a, const qreal& b) noexcept;
    friend TMatrix operator+(TMatrix a, const qreal& b) noexcept;
    friend TMatrix operator-(TMatrix a, const qreal& b) noexcept;

protected:
    alglib::real_2d_array Data;
    size_t Rows;
    size_t Columns;
};

class TVector : public TMatrix {
public:
    TVector() = default;

    static TVector MakeUnitaryVector(const size_t length) noexcept {
        return TMatrix(length, 1, 1);
    }

    TVector(const TVector& other) noexcept
        : TMatrix(other)
    {}

    TVector(const TMatrix& other)  noexcept
        : TMatrix(other)
    {}

    TVector(const size_t length, const qreal value = 0) noexcept
        : TMatrix(length, 1, value)
    {}

    qreal operator[](const size_t i) const noexcept {
        return Data[i][0];
    }

    qreal& operator[](const size_t i) noexcept {
        return Data[i][0];
    }

    size_t GetLength() const noexcept {
        return Rows;
    }

    qreal operator ()(const TVector& v) const; // dot product
    TVector& operator*=(const qreal& mult) noexcept;
    TVector& operator/=(const qreal& mult) noexcept;

    TVector& operator+=(const TVector& m);
    TVector& operator-=(const TVector& m);
    TVector& operator+=(const qreal& m) noexcept;
    TVector& operator-=(const qreal& m) noexcept;
    TVector& operator<<(const qreal& v) noexcept;
    TVector& operator<<(const TVector& v) noexcept; // concat vectors

    friend TVector operator+(TVector a, const TVector& b) noexcept;
    friend TVector operator-(TVector a, const TVector& b) noexcept;
    friend TVector operator*(TVector a, const qreal& b) noexcept;
    friend TVector operator/(TVector a, const qreal& b) noexcept;
    friend TVector operator+(TVector a, const qreal& b) noexcept;
    friend TVector operator-(TVector a, const qreal& b) noexcept;
};
