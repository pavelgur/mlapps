#pragma once

#include "lib.h"
#include <QDebug>
#include <QColor>

class IAlgorithm {
public:
    IAlgorithm(const TSample& learnSet)
        : LearnSet(learnSet)
        , Trained(false)
    {
        if (LearnSet.empty())
            throw myexception() << "empty learn set";
    }
    virtual ~IAlgorithm() {}

    virtual void Train() {
        Trained = true;
        qDebug() << GetName() << "Learning finished!";
    }
    virtual qreal Apply(const TVector &x) const noexcept = 0;
    virtual QString GetName() const = 0;
    bool IsTrained() const {
        return Trained;
    }

protected:
    const TSample& LearnSet;
private:
    bool Trained;
};
