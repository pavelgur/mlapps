#include "tdecisionforest.h"

#include <QtMath>

TDecisionForest::TDecisionForest(const TSample& learnSet, const size_t iterations = 10000)
    : IAlgorithm(learnSet)
    , Iterations(iterations)
{}

qreal TDecisionForest::Apply(const TVector& x) const {
    if (!IsTrained())
        return 0;
    return (int) Qt::white;
}

TVector TDecisionForest::LearnBase() const {
    QVector<bool> isChosen(LearnSet.size(), false);
    srand(time(0));
    for (size_t i = 0; i < (size_t) qMin(isChosen.size(), qMax(10, isChosen.size() / 2)); ++i) {
        isChosen[(size_t) isChosen.size() * (qreal) rand() / RAND_MAX] = true;
    }
    for (size_t feature = 0; feature < LearnSet.front().first.GetLength(); ++feature) {
        for (size_t i = 0; i < (size_t) LearnSet.size(); ++i) {
            if (!isChosen[i])
                continue;
        }
    }
}

qreal TDecisionForest::GoldenSearch(const size_t feature, const QVector<bool> &isChosen) const {
    static const qreal phi = (1 + qSqrt(5)) / 2;
    qreal a = 0, b = 1;
    qreal x1 = b - (b - a) / phi, x2 = a + (b - a) / phi;
}

static qreal LogisticFoo(const qreal& x) {
    return 1.0 / (1.0 + qExp(-x));
}

void TDecisionForest::Train() {
    uint classCount = 0;
    {
        QSet<qreal> classes;
        for (const auto& pair : LearnSet) {
            classes.insert(pair.second);
        }
        classCount = classes.size();
        if (classCount == 0) {
            qDebug() << "Learn set is empty";
            return;
        }
    }
    QMap<qreal, TVector> thetas; // векторы, который нужны для определения вероятности принадлежности точки к классам (ключам)
    { // logist boost
        for (int countDown = 0; countDown < 10000; ++countDown) {
            QMap<qreal, TVector> deltas;
            for (const auto& pair : LearnSet) {
                auto it = deltas.find(pair.second);
                if (it == deltas.end())
                    it = deltas.insert(pair.second, TVector(LearnSet.front().first.GetLength(), 0));
                it.value() += (pair.first * (pair.second - LogisticFoo(thetas(pair.first)))) * 0.1;
            }
            qreal delta = 0;
            for (auto it = deltas.begin(); it != deltas.end(); ++it) {
                auto iter = thetas.find(it.key());
                if (iter == thetas.end())
                    iter = thetas.insert(it.key(), TVector(LearnSet.front().first.GetLength(), 0));
                iter.value() += it.value();
                delta = qMax(delta, it.value()(it.value()));
            }
            if (delta < 0.01)
                break;
        }
    }
    for (size_t curIteration = 0; curIteration < Iterations; ++curIteration) {

    }

    IAlgorithm::Train();
}

qreal TDecisionForest::Psi(const TVector& theta, const size_t feature, const qreal& x, const QVector<bool>& isChosen) const { // loss function
    qreal sum = 0;
    for (size_t i = 0; i < LearnSet.size(); ++i) {
        if (!isChosen[i])
            continue;
//        sum -= 0;
    }
}
