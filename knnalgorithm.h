#pragma once

#include "lib.h"

class TKnnAlgorithm : public IAlgorithm
{
public:
    TKnnAlgorithm(const TSample& learnSet, const size_t k = 6);
    virtual ~TKnnAlgorithm() {}

    QString GetName() const noexcept override {
        return "kNN classficaion";
    }

    void Train() override {IAlgorithm::Train();}
    qreal Apply(const TVector &x) const noexcept override;

private:
    const size_t K;
};
