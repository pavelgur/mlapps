#pragma once

#include "IAlgorithm.h"

class TDecisionForest : public IAlgorithm {
public:
    TDecisionForest(const TSample& learnSet, const size_t iterations);
    QString GetName() const override {
        return "Decision Forest";
    }
    void Train() override;
    qreal Apply(const TVector& x) const override;
    virtual ~TDecisionForest() {}

private:
    struct TTree {
        TTree(const size_t dim = 2)
            : Rules(Height, TVector(dim, 0))
            , Weight(1)
        {}

        qreal Apply(const TVector& features) const {
            for (const TVector& rule : Rules) {
                if (features(rule) < 0) {
                    return 0;
                }
            }
            return Weight;
        }

        QVector<TVector> Rules;
        qreal Weight;
        static const size_t Height = 6;
    };

    struct TForest {
        TForest& operator+= (const TTree& tree) {
            Trees.push_back(tree);
            return *this;
        }
        QVector<TTree> Trees;
    };

private:
    TVector LearnBase() const;
    qreal GoldenSearch(const size_t feature, const QVector<bool>& isChosen) const;
    qreal Psi(const TVector &theta, const size_t feature, const qreal& x, const QVector<bool>& isChosen) const;
    const size_t Iterations;
};
