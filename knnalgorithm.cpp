#include "knnalgorithm.h"
#include <map>
#include <QRgb>

TKnnAlgorithm::TKnnAlgorithm(const TSample &learnSet, const size_t k)
    : IAlgorithm(learnSet)
    , K(k)
{
    if ((size_t) LearnSet.size() < K + 1)
        throw myexception() << "too small learn set";
}

qreal TKnnAlgorithm::Apply(const TVector &x) const noexcept {
    QMap<qreal, qreal> store; // dist to class
    for (const auto& pair : LearnSet) {
        const TVector v = x - pair.first;
        store.insertMulti(v(v), pair.second);
    }
    QMap<qreal, uint> counter;
    int objectsLeft = K;
    for (const auto& val : store) {
        counter[val] += 1;
        --objectsLeft;
        if (objectsLeft == 0)
            break;
    }
    return [&] () {
        auto res = static_cast<qreal>(Qt::white);
        auto bestCount = 0u;
        for (auto it = counter.begin(); it != counter.end(); ++it) {
            if (bestCount < it.value()) {
                res = it.key();
                bestCount = it.value();
            }
        }
        return res;
    } ();
}
