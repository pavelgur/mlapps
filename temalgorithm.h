#pragma once

#include "lib.h"
#include <QHash>

/**
 * @brief The TEmAlgorithm class
 * Стохастический EM-алгоритм с автоматическим выбором компонент.
 * Каждый класс есть совокупность n-мерных гауссоид.
 */
class TEmAlgorithm : public IAlgorithm {
    struct TGaussianDist {
        TGaussianDist(const size_t dim = 1)
            : Mu(dim)
            , Sigma(dim, dim)
        {}

        TVector Mu;
        TMatrix Sigma;
    };
    struct TClassData {
        QVector<const TVector*> Dots;
        QVector<TGaussianDist> Theta;
        size_t MinObjectLeft;
        QVector<qreal> Omega;
        bool Calculated {false};
        qreal ClassPower {0.0};
    };

public:
    TEmAlgorithm(const TSample& learnSet);
    virtual ~TEmAlgorithm() {}

    QString GetName() const noexcept override {
        return "EM-regression";
    }
    void Train() override;
    qreal Apply(const TVector &x) const noexcept override;

private:
    inline void AddComponent(const qreal& newComponentWeight, const QVector<const TVector*> &dots, TEmAlgorithm::TClassData& data);
    inline void Optimize(const QVector<const TVector*>& dots, TEmAlgorithm::TClassData& data, const size_t j, const QVector<bool>& skipPoint);
    inline qreal ObjectLikelihood(const TClassData &data, const TVector &x) const noexcept;
    inline qreal Phi(const TVector& x, const TGaussianDist& dist) const;

private:
    const size_t Dimension;
    static constexpr auto Delta = 0.01;
    static constexpr auto StopCoef = 10;

    QHash<qreal, TClassData> ClassToData;
};
