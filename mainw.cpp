#include "mainw.h"
#include "temalgorithm.h"
#include "tsvmalgorithm.h"
#include "knnalgorithm.h"
#include "lib.h"
#include "ui_mainw.h"

#include <QVector>
#include <cstdlib>
#include <ctime>
#include <QtMath>
#include <QDebug>
#include <QMouseEvent>
#include <QScopedPointer>
#include <QGraphicsView>

MainW::MainW(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainW)
{
    ui->setupUi(this);
    connect(ui->graphicsView, SIGNAL(NewPoint(QPointF,Qt::GlobalColor)), this, SLOT(GetPoint(QPointF,Qt::GlobalColor)));
    connect(ui->RunButton, SIGNAL(clicked()), this, SLOT(RunAlgo()));
    connect(ui->ResetButton, SIGNAL(clicked()), ui->graphicsView, SLOT(Clear()));
    connect(ui->ResetButton, SIGNAL(clicked()), this, SLOT(Clear()));

    ButtonGroup.setExclusive(true);
    ButtonGroup.addButton(ui->RedButton, (int) Qt::red);
    ButtonGroup.addButton(ui->GreenButton, (int) Qt::green);
    ButtonGroup.addButton(ui->YellowButton, (int) Qt::yellow);
    ButtonGroup.addButton(ui->GrayButton, (int) Qt::gray);
    ButtonGroup.addButton(ui->BlueButton, (int) Qt::blue);
    ButtonGroup.addButton(ui->DarkBlueButton, (int) Qt::darkBlue);
    connect(&ButtonGroup, SIGNAL(buttonClicked(int)), ui->graphicsView, SLOT(SetColor(int)));
    connect(ui->algoChoice, SIGNAL(currentIndexChanged(int)), this, SLOT(ChangeAlgo(int)));
}

void MainW::Clear() {
    Sample.clear();
}

void MainW::RunAlgo() {
    QScopedPointer<IAlgorithm> algo;
    switch (CurAlgoChoice) {
    case A_EmRegression:
        algo.reset(new TEmAlgorithm(Sample));
        break;
    case A_KnnClassification:
        algo.reset(new TKnnAlgorithm(Sample));
        break;
    case A_RbfSvmRegression:
        algo.reset(new TSvmAlgorithm(Sample));
        break;
    }
    algo->Train();

    const QPointF topLeft = ui->graphicsView->rect().topLeft();
    const QPointF bottomRight = ui->graphicsView->rect().bottomRight();
    for (qreal x = topLeft.x(); x <= bottomRight.x(); x += 1) {
        for (qreal y = topLeft.y(); y <= bottomRight.y(); y += 1) {
            const QPointF point = ui->graphicsView->mapToScene(x, y);
            TVector v(2);
            v[0] = point.x();
            v[1] = point.y();
            ui->graphicsView->AddAreaPoint(point, static_cast<Qt::GlobalColor>(algo->Apply(v)));
        }
    }
    ui->graphicsView->Redraw();
}

void MainW::GetPoint(const QPointF& pos, const Qt::GlobalColor color) {
    TVector v(2, 0);
    v[0] = pos.x();
    v[1] = pos.y();
    Sample.push_back(QPair<TVector, qreal>(v, (qreal)color));
}

MainW::~MainW() {
    delete ui;
}
